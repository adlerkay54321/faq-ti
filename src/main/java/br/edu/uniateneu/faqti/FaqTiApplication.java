package br.edu.uniateneu.faqti;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FaqTiApplication {

	public static void main(String[] args) {
		SpringApplication.run(FaqTiApplication.class, args);
	}

}
