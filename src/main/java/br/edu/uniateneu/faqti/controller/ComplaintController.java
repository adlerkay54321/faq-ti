/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */

package br.edu.uniateneu.faqti.controller;

/**
 *
 * @author missi
 */
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.edu.uniateneu.faqti.model.Complaint;
import br.edu.uniateneu.faqti.service.ComplaintService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;

@RestController
@RequestMapping("/complaint")

public class ComplaintController {
 @Autowired
    ComplaintService complaintService;
    
    @GetMapping
    @Operation(summary="Listar todos os usuários", description  = "Listagem de Usuários")
    @ApiResponses(
        value = {
            @ApiResponse(responseCode = "200", description = "A requisição foi executada com sucesso."),
            @ApiResponse(responseCode = "400", description = "Requisição Inválida"), 
            @ApiResponse(responseCode = "403", description = "Você não tem permissão para acessar esse recurso."), 
            @ApiResponse(responseCode = "404", description = "Recurso não encontrado.")})
    
    public ResponseEntity<List<Complaint>> getAll(){
        return new ResponseEntity<>(complaintService.getAll(), HttpStatus.OK);
    } 

    @GetMapping("/{id}")
    public ResponseEntity<Complaint> getById(@PathVariable Long id){
       Complaint complaint = complaintService.getById(id);
        if(complaint != null){
            return new ResponseEntity<>(complaint, HttpStatus.OK);
        }else{
            return null;
        }
    }

    @PostMapping
    public ResponseEntity<Complaint> saveComplaint(@RequestBody Complaint complaint){
        return new ResponseEntity<>(complaintService.saveComplaint(complaint), HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Complaint> updateComplaint(@PathVariable Long id, @RequestBody Complaint complaint){
        Complaint complaintUpdated = complaintService.updateComplaint(id, complaint);
        if(complaintUpdated != null){
            return new ResponseEntity<>(complaintUpdated, HttpStatus.OK);
        }else{
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteComplaint(@PathVariable Long id){
        if(complaintService.deleteComplaint(id)){
            return new ResponseEntity<>(HttpStatus.OK);
        }else{
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

}
