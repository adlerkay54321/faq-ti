package br.edu.uniateneu.faqti.controller;

import java.util.*;

import br.edu.uniateneu.faqti.model.Response;
import br.edu.uniateneu.faqti.service.ResponseService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/response")
public class ResponseController {
    @Autowired
    ResponseService responseService;
    
    @GetMapping
    @Operation(summary="Listar todos os usuários", description  = "Listagem de Usuários")
    @ApiResponses(
        value = {
            @ApiResponse(responseCode = "200", description = "A requisição foi executada com sucesso."),
            @ApiResponse(responseCode = "400", description = "Requisição Inválida"), 
            @ApiResponse(responseCode = "403", description = "Você não tem permissão para acessar esse recurso."), 
            @ApiResponse(responseCode = "404", description = "Recurso não encontrado.")})
    
    public ResponseEntity<List<Response>> getAll(){
        return new ResponseEntity<>(responseService.getAll(), HttpStatus.OK);
    } 

    @GetMapping("/{id}")
    public ResponseEntity<Response> getById(@PathVariable Long id){
        Response response = responseService.getById(id);
        if(response != null){
            return new ResponseEntity<>(response, HttpStatus.OK);
        }else{
            return null;
        }
    }

    @PostMapping
    public ResponseEntity<Response> saveUser(@RequestBody Response response){
        return new ResponseEntity<>(responseService.saveResponse(response), HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Response> updateResponse(@PathVariable Long id, @RequestBody Response response){
        Response responseUpdated = responseService.updateResponse(id, response);
        if(responseUpdated != null){
            return new ResponseEntity<>(responseUpdated, HttpStatus.OK);
        }else{
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteResponse(@PathVariable Long id){
        if(responseService.deleteResponse(id)){
            return new ResponseEntity<>(HttpStatus.OK);
        }else{
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}

