/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package br.edu.uniateneu.faqti.controller;

/**
 *
 * @author adler
 */
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.edu.uniateneu.faqti.model.Forum;
import br.edu.uniateneu.faqti.service.ForumService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;

@RestController
@RequestMapping("/forum")
public class ForumController {
    
    @Autowired
    ForumService forumService;
    
    @GetMapping
    @Operation(summary="Listar todos os usuários", description  = "Listagem de Usuários")
    @ApiResponses(
        value = {
            @ApiResponse(responseCode = "200", description = "A requisição foi executada com sucesso."),
            @ApiResponse(responseCode = "400", description = "Requisição Inválida"), 
            @ApiResponse(responseCode = "403", description = "Você não tem permissão para acessar esse recurso."), 
            @ApiResponse(responseCode = "404", description = "Recurso não encontrado.")})
    
    public ResponseEntity<List<Forum>> getAll(){
        return new ResponseEntity<>(forumService.getAll(), HttpStatus.OK);
    } 

    @GetMapping("/{id}")
    public ResponseEntity<Forum> getById(@PathVariable Long id){
       Forum forum = forumService.getById(id);
        if(forum != null){
            return new ResponseEntity<>(forum, HttpStatus.OK);
        }else{
            return null;
        }
    }

    @PostMapping
    public ResponseEntity<Forum> saveForum(@RequestBody Forum forum){
        return new ResponseEntity<>(forumService.saveForum(forum), HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Forum> updateForum(@PathVariable Long id, @RequestBody Forum forum){
        Forum forumUpdated = forumService.updateForum(id, forum);
        if(forumUpdated != null){
            return new ResponseEntity<>(forumUpdated, HttpStatus.OK);
        }else{
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteForum(@PathVariable Long id){
        if(forumService.deleteForum(id)){
            return new ResponseEntity<>(HttpStatus.OK);
        }else{
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}

