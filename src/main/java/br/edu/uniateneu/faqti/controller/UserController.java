package br.edu.uniateneu.faqti.controller;

import java.util.*;

import br.edu.uniateneu.faqti.model.Users;
import br.edu.uniateneu.faqti.service.UserService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/users")
public class UserController {
    @Autowired
    UserService userService;
    
    @GetMapping
    @Operation(summary="Listar todos os usuários", description  = "Listagem de Usuários")
    @ApiResponses(
        value = {
            @ApiResponse(responseCode = "200", description = "A requisição foi executada com sucesso."),
            @ApiResponse(responseCode = "400", description = "Requisição Inválida"), 
            @ApiResponse(responseCode = "403", description = "Você não tem permissão para acessar esse recurso."), 
            @ApiResponse(responseCode = "404", description = "Recurso não encontrado.")})
    
    public ResponseEntity<List<Users>> getAll(){
        return new ResponseEntity<>(userService.getAll(), HttpStatus.OK);
    } 

    @GetMapping("/{id}")
    public ResponseEntity<Users> getById(@PathVariable Long id){
        Users user = userService.getById(id);
        if(user != null){
            return new ResponseEntity<>(user, HttpStatus.OK);
        }else{
            return null;
        }
    }

    @PostMapping
    public ResponseEntity<Users> saveUser(@RequestBody Users user){
        return new ResponseEntity<>(userService.saveUser(user), HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Users> updateUser(@PathVariable Long id, @RequestBody Users user){
        Users userUpdated = userService.updateUser(id, user);
        if(userUpdated != null){
            return new ResponseEntity<>(userUpdated, HttpStatus.OK);
        }else{
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteUser(@PathVariable Long id){
        if(userService.deleteUser(id)){
            return new ResponseEntity<>(HttpStatus.OK);
        }else{
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}
