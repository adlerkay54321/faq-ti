package br.edu.uniateneu.faqti.controller;

import java.util.*;

import br.edu.uniateneu.faqti.model.Question;
import br.edu.uniateneu.faqti.service.QuestionService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/question")

public class QuestionController {
    @Autowired
    QuestionService questionService;
    
    @GetMapping
    @Operation(summary="Listar todos os usuários", description  = "Listagem de Usuários")
    @ApiResponses(
        value = {
            @ApiResponse(responseCode = "200", description = "A requisição foi executada com sucesso."),
            @ApiResponse(responseCode = "400", description = "Requisição Inválida"), 
            @ApiResponse(responseCode = "403", description = "Você não tem permissão para acessar esse recurso."), 
            @ApiResponse(responseCode = "404", description = "Recurso não encontrado.")})
    
    public ResponseEntity<List<Question>> getAll(){
        return new ResponseEntity<>(questionService.getAll(), HttpStatus.OK);
    } 

    @GetMapping("/{id}")
    public ResponseEntity<Question> getById(@PathVariable Long id){
        Question question = questionService.getById(id);
        if(question != null){
            return new ResponseEntity<>(question, HttpStatus.OK);
        }else{
            return null;
        }
    }

    @PostMapping
    public ResponseEntity<Question> saveQuestion(@RequestBody Question question){
        return new ResponseEntity<>(questionService.saveQuestion(question), HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Question> updateQuestion(@PathVariable Long id, @RequestBody Question question){
        Question questionUpdated = questionService.updateQuestion(id, question);
        if(questionUpdated != null){
            return new ResponseEntity<>(questionUpdated, HttpStatus.OK);
        }else{
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteQuestion(@PathVariable Long id){
        if(questionService.deleteQuestion(id)){
            return new ResponseEntity<>(HttpStatus.OK);
        }else{
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}
 