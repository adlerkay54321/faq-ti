/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package br.edu.uniateneu.faqti.model;

/**
 *
 * @author adler
 */
import jakarta.persistence.*;


@Entity
@Table(name="foruns")
public class Forum {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY )         
    @Column(name="forum_id")
    private Long forumId;
    
    @Column(name="user_id")
    private Long userId;
    
          @Column(name="forum_create")
    private String forumCreate;

                  public Long getForumId(){
        return forumId;
    }
    public void setForumId(Long forumId){
        this.forumId = forumId;
         }
     public Long getUserId(){
        return userId;
    }
    public void setUserId(Long userId){
        this.userId = userId;
    }
     public String getForumCreate(){
        return forumCreate;
    }
    public void setForumCreate(String forumCreate){
        this.forumCreate = forumCreate;
    }
  
    
   
    
}
