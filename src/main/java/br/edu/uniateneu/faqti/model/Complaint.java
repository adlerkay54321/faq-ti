package br.edu.uniateneu.faqti.model;

import jakarta.persistence.*;


@Entity
@Table(name="complaints")

public class Complaint {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY )         
    @Column(name="complaint_id")
    private Long complaintId;

    @Column(name="user_id")
    private Long userId;

    @Column(name="complaint_create")
    private String complaintCreate;

    public Long getComplaintId(){
        return complaintId;
    }
    public void setComplaintId(Long complaintId){
        this.complaintId = complaintId;
    }
    public Long getUserId(){
        return userId;
    }
    public void setUserId(Long userId){
        this.userId = userId;
    }
    public String getComplaintCreate(){
        return complaintCreate;
    }
    public void setComplaintCreate(String complaintCreate){
        this.complaintCreate = complaintCreate;
    }
  }
