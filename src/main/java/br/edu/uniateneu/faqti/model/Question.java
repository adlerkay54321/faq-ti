package br.edu.uniateneu.faqti.model;

import jakarta.persistence.*;


@Entity
@Table(name="Questions")
public class Question {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY )         
    @Column(name="question_id")
    private Integer questionId;

    @Column(name="user_id")
    private Integer userId;

    @Column(name="create_question")
    private String createQuestion;

    public Integer getQuestionId(){
        return questionId;
    }
    public void setQuestionId(Integer questionId){
        this.questionId = questionId;
         }
         public Integer getUserId(){
            return userId;
        }
        public void setUserId(Integer userId){
            this.userId = userId;
        }
        public String getCreateQuestion(){
            return createQuestion;
        }
        public void setCreateQuestion(String createQuestion){
            this.createQuestion = createQuestion;
        }
         
}
