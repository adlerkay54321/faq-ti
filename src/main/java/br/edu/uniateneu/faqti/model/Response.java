package br.edu.uniateneu.faqti.model;

import jakarta.persistence.*;


@Entity
@Table(name="Responses")

public class Response {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY )         
    @Column(name="response_id")
    private Integer responseId;

    @Column(name="user_id")
    private Integer userId;

    @Column(name="create_response")
    private String createResponse;

    public Integer getResponseId(){
        return responseId;
    }
    public void setResponseId(Integer responseId){
        this.responseId = responseId;
         }
         public Integer getUserId(){
            return userId;
        }
        public void setUserId(Integer userId){
            this.userId = userId;
        }
        public String getCreateResponse(){
            return createResponse;
        }
        public void setCreateResponse(String createResponse){
            this.createResponse = createResponse;
        }
        
}
