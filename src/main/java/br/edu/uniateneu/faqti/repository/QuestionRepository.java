package br.edu.uniateneu.faqti.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.edu.uniateneu.faqti.model.Question;
@Repository
public interface QuestionRepository extends JpaRepository<Question, Long>{
    
}
