package br.edu.uniateneu.faqti.repository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.edu.uniateneu.faqti.model.Response;

@Repository
public interface ResponseRepository extends JpaRepository<Response, Long>{

    
}


