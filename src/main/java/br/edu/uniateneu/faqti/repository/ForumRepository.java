/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package br.edu.uniateneu.faqti.repository;

/**
 *
 * @author adler
 */
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.edu.uniateneu.faqti.model.Forum;
@Repository
public interface ForumRepository extends JpaRepository<Forum, Long>{
    
}
