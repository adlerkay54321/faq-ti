/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */

package br.edu.uniateneu.faqti.repository;

/**
 *
 * @author missi
 */
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.edu.uniateneu.faqti.model.Complaint;
@Repository
public interface ComplaintRepository extends JpaRepository<Complaint, Long>{
    
}
