package br.edu.uniateneu.faqti.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.edu.uniateneu.faqti.model.Response;

import br.edu.uniateneu.faqti.repository.ResponseRepository;

@Service       
public class ResponseService {
 @Autowired
    ResponseRepository responseRepository;

    public List<Response> getAll(){
        return responseRepository.findAll();
    }

    public Response getById(Long id) {
        return responseRepository.findById(id).orElse(null) ;
    }

    public Response saveResponse(Response response) {
        return responseRepository.saveAndFlush(response);
    }

    public Response updateResponse(Long id, Response Response) {
        Response currentResponse = responseRepository.findById(id).orElse(null);
        if(currentResponse != null) {
            currentResponse.setResponseId(Response.getResponseId());
            currentResponse.setUserId(Response.getUserId());
            currentResponse.setCreateResponse(Response.getCreateResponse());
           
            return responseRepository.save(currentResponse);
        }else {
            return null;
        }
    }

    public Boolean deleteResponse(Long id) {
        Response response = responseRepository.findById(id).orElse(null);
        if(response != null) {
            responseRepository.delete(response);
            return true;
        }else {
            return false;
        }
    }

}
