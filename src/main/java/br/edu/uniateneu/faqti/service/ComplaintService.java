/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */

package br.edu.uniateneu.faqti.service;

/**
 *
 * @author missi
 */
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.edu.uniateneu.faqti.model.Complaint;
import br.edu.uniateneu.faqti.repository.ComplaintRepository;


@Service

public class ComplaintService {
@Autowired
    ComplaintRepository complaintRepository;

    public List<Complaint> getAll(){
        return complaintRepository.findAll();
    }

    public Complaint getById(Long id) {
        return complaintRepository.findById(id).orElse(null) ;
    }

    public Complaint saveComplaint(Complaint complaint) {
        return complaintRepository.save(complaint);
    }

    public Complaint updateComplaint(Long id, Complaint complaint) {
       Complaint currentComplaint = complaintRepository.findById(id).orElse(null);
        if(currentComplaint != null) {
            currentComplaint.setComplaintId(complaint.getComplaintId());
            currentComplaint.setUserId(complaint.getUserId());
            currentComplaint.setComplaintCreate(complaint.getComplaintCreate());
            
           
            return complaintRepository.save(currentComplaint);
        }else {
            return null;
        }
    }

    public Boolean deleteComplaint(Long id) {
        Complaint complaint = complaintRepository.findById(id).orElse(null);
        if(complaint != null) {
           complaintRepository.delete(complaint);
            return true;
        }else {
            return false;
        }
    }
   
    


}
