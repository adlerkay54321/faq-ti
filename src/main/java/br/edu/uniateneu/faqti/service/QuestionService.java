package br.edu.uniateneu.faqti.service;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.edu.uniateneu.faqti.model.Question;
import br.edu.uniateneu.faqti.repository.QuestionRepository;

@Service       
public class QuestionService {
    @Autowired
    QuestionRepository questionRepository;

    public List<Question> getAll(){
        return questionRepository.findAll();
    }

    public Question getById(Long id) {
        return questionRepository.findById(id).orElse(null) ;
    }

    public Question saveQuestion(Question question) {
        return questionRepository.saveAndFlush(question);
    }

    public Question updateQuestion(Long id, Question question) {
        Question currentQuestion = questionRepository.findById(id).orElse(null);
        if(currentQuestion != null) {
            currentQuestion.setQuestionId(question.getQuestionId());
            currentQuestion.setUserId(question.getUserId());
            currentQuestion.setCreateQuestion(question.getCreateQuestion());
            
            return questionRepository.save(currentQuestion);
        }else {
            return null;
        }
    }

    public Boolean deleteQuestion(Long id) {
        Question question = questionRepository.findById(id).orElse(null);
        if(question != null) {
            questionRepository.delete(question);
            return true;
        }else {
            return false;
        }
    }

}


